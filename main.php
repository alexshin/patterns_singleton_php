<?php

	use Singleton\Preferences;
	
	require_once "vendor/autoload.php";

	$preferences = Preferences::getInstance();
	$preferences->setProperty("name", "Alex Shinkevich");

	unset($preferences);

	$preferences2 = Preferences::getInstance();
	print $preferences2->getProperty("name");