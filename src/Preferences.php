<?php

namespace Singleton;


class Preferences {
	private $props = [];
	private static $instance = null;


	private function __construct(){}
	private final function __clone(){}


	public static function getInstance(){
		if (is_null(self::$instance)){
			self::$instance = new Preferences;
		}
		return self::$instance;
	}


	public function setProperty($key, $val){
		$this->props[$key] = $val;
	}


	public function getProperty($key){
		return $this->props[$key];
	}
}